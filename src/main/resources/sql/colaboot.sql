SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_access
-- ----------------------------
DROP TABLE IF EXISTS `sys_access`;
CREATE TABLE `sys_access`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `access_name` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名字',
  `access_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限的规则',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '权限父级id access_parent',
  `menu_level` int(11) NULL DEFAULT NULL COMMENT '菜单等级(0:方法；1:一级；2:2级；)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_access
-- ----------------------------
INSERT INTO `sys_access` VALUES (1, '商家管理', NULL, 0, 1);
INSERT INTO `sys_access` VALUES (2, '客户管理', NULL, 0, 1);
INSERT INTO `sys_access` VALUES (3, '商家列表', NULL, 1, 2);
INSERT INTO `sys_access` VALUES (4, '查看商家', '/findAll', 3, 0);
INSERT INTO `sys_access` VALUES (5, '管理员管理', NULL, 0, 1);
INSERT INTO `sys_access` VALUES (6, '角色管理', NULL, 5, 2);
INSERT INTO `sys_access` VALUES (7, '权限管理', NULL, 5, 2);
INSERT INTO `sys_access` VALUES (8, '用户列表', NULL, 5, 2);
INSERT INTO `sys_access` VALUES (9, '添加角色', '/user/getById', 6, 0);
INSERT INTO `sys_access` VALUES (10, '编辑角色', '/role/editRole', 6, 0);
INSERT INTO `sys_access` VALUES (11, '添加权限', '/access/addAccess', 7, 0);
INSERT INTO `sys_access` VALUES (12, '删除权限', '/access/delAccess', 7, 0);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名',
  `role_desc` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', '上帝视角');

-- ----------------------------
-- Table structure for sys_role_access
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_access`;
CREATE TABLE `sys_role_access`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `access_id` int(11) NULL DEFAULT NULL COMMENT '权限ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_access
-- ----------------------------
INSERT INTO `sys_role_access` VALUES (1, 1, 1);
INSERT INTO `sys_role_access` VALUES (2, 1, 2);
INSERT INTO `sys_role_access` VALUES (3, 1, 3);
INSERT INTO `sys_role_access` VALUES (4, 1, 4);
INSERT INTO `sys_role_access` VALUES (5, 1, 5);
INSERT INTO `sys_role_access` VALUES (6, 1, 6);
INSERT INTO `sys_role_access` VALUES (7, 1, 7);
INSERT INTO `sys_role_access` VALUES (8, 1, 8);
INSERT INTO `sys_role_access` VALUES (9, 1, 9);
INSERT INTO `sys_role_access` VALUES (10, 1, 10);
INSERT INTO `sys_role_access` VALUES (11, 1, 11);
INSERT INTO `sys_role_access` VALUES (12, 1, 12);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '000000' COMMENT '密码',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `enabled` tinyint(2) NULL DEFAULT NULL COMMENT '是否禁用1是0否',
  `role_id` int(10) NOT NULL COMMENT '权限ID',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_name`(`username`) USING BTREE,
  UNIQUE INDEX `user_phone`(`phone`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1001', 'admin', '$2a$10$MxxYsl3zANgsSCxJpsQazeVhA/K4H23w2A/ZCLz8LQM/qogdCa6vy', '18123416815', 'colaiven@aliyun.com', NULL, 1, '2019-11-14 11:52:22');
INSERT INTO `sys_user` VALUES ('1002', 'admin2', '$2a$10$MxxYsl3zANgsSCxJpsQazeVhA/K4H23w2A/ZCLz8LQM/qogdCa6vy', NULL, NULL, NULL, 1, '2019-12-05 15:40:34');

SET FOREIGN_KEY_CHECKS = 1;
