package com.cola.colaboot.config.security;

import com.cola.colaboot.module.system.pojo.SysUser;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {

    public static SysUser currentUser(){
        return (SysUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
