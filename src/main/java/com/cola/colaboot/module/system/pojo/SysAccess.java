package com.cola.colaboot.module.system.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@TableName("sys_access")
public class SysAccess implements GrantedAuthority, Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String accessName;
    private String accessUrl;
    private String component;
    private Integer parentId;
    private Integer menuLevel;
    private String menuIcon;
    private Integer sortNum;


    @TableField(exist = false)
    private String authority;
    @TableField(exist = false)
    private List<SysAccess> children = new ArrayList<>();//赋值初始值，避免vue-TreeSelect抛出异常
    @TableField(exist = false)
    private boolean hasChildren = true;
    @TableField(exist = false)
    private String label;//vue-TreeSelect组件标识字段，等同于accessName
}