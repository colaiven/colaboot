package com.cola.colaboot.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cola.colaboot.module.system.pojo.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser> {
}
