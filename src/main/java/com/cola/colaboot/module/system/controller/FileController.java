package com.cola.colaboot.module.system.controller;

import com.cola.colaboot.config.dto.Res;
import com.cola.colaboot.config.exception.ColaException;
import com.cola.colaboot.module.system.pojo.SysDict;
import com.cola.colaboot.module.system.service.SysDictService;
import com.cola.colaboot.utils.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@RestController
@RequestMapping("/file")
public class FileController {
    @Autowired
    private SysDictService dictService;

    @PostMapping("/upload")
    public Res<?> upload(@RequestParam("file") MultipartFile multipartFile,@RequestParam("dir") String dir){
        SysDict uploadPath = dictService.getByTag("FILE_UPLOAD_ROOT");
        if (uploadPath == null) throw new ColaException("FILE_UPLOAD_ROOT未配置");
        String sourceUrl = "image/"+dir+"/";
        File file = FileUtil.saveFile(uploadPath.getDictValue() + dir, "", multipartFile);
        return Res.ok("/"+sourceUrl+file.getName());
    }
}
